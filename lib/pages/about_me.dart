import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:migo_test/components/constants.dart';
import 'package:migo_test/widgets/global_header.dart';
import 'package:migo_test/widgets/img_card.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutMePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).bottomAppBarColor,
      child: SafeArea(
        top: false,
        child: Scaffold(
          appBar: globalAppBar('Обо мне'),
          drawer: GlobalDrawer(),
          body: Stack(
            alignment: AlignmentDirectional.bottomCenter,
            children: [
              ListView(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      children: [
                        const Text(
                          'Игорь Ожиганов\n23 года',
                          textAlign: TextAlign.center,
                        ),
                        Divider(
                          indent: MediaQuery.of(context).size.width / 5,
                          endIndent: MediaQuery.of(context).size.width / 5,
                        ),
                        Container(
                          padding: const EdgeInsets.all(5),
                          child: CircleAvatar(
                            backgroundImage: NetworkImage(my_img_url),
                            radius: MediaQuery.of(context).size.width / 4,
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width,
                          child: Card(
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Column(
                                children: const [
                                  Text(
                                    'магистр\n(телекоммуникации)',
                                    textAlign: TextAlign.center,
                                  ),
                                  Divider(),
                                  Text(
                                    'До Flutter разработки занимался видеоинженерией, интерактивными инсталляциями, онлайн-трансляциями и т.п.',
                                    textAlign: TextAlign.justify,
                                  ),
                                  Divider(),
                                  Text(
                                    'Так же преподаю TouchDesigner и записываю по нему уроки (на YouTube)',
                                    textAlign: TextAlign.justify,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                            child: SizedBox(
                          height: 150,
                          child: GridView.count(
                            scrollDirection: Axis.horizontal,
                            crossAxisCount: 1,
                            mainAxisSpacing: 5,
                            children: List.generate(images.length, (i) {
                              return ImgCard(
                                url: images[i],
                              );
                            }),
                          ),
                        )),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 50,
                  )
                ],
              ),
              Container(
                height: 50,
                color: Theme.of(context).bottomAppBarColor,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                      onPressed: () {
                        launch(my_youtube_url);
                      },
                      icon: const Icon(FontAwesomeIcons.youtube),
                    ),
                    IconButton(
                      onPressed: () {
                        launch(my_vk_url);
                      },
                      icon: const Icon(FontAwesomeIcons.vk),
                    ),
                    IconButton(
                      onPressed: () {
                        launch(my_instagram_url);
                      },
                      icon: const Icon(FontAwesomeIcons.instagram),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
