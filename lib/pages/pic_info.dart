import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:migo_test/bloc/picBloc/pic_bloc.dart';
import 'package:migo_test/bloc/picBloc/pic_event.dart';
import 'package:migo_test/bloc/picBloc/pic_state.dart';
import 'package:migo_test/models/pic.dart';
import 'package:migo_test/widgets/global_header.dart';

class PicInfoPage extends StatelessWidget {
  const PicInfoPage({@required this.pic});

  final Pic pic;

  @override
  Widget build(BuildContext context) {
    final PicBloc picBloc = BlocProvider.of<PicBloc>(context);
    picBloc.add(PicLoadEvent());

    return Scaffold(
      appBar: globalAppBar('Галерея'),
      drawer: GlobalDrawer(),
      body: BlocConsumer(
        bloc: picBloc,
        listener: (context, state) {},
        builder: (context, state) {
          if (state is PicLoadingState) {
            return const Center(
              child: CircularProgressIndicator.adaptive(),
            );
          }
          if (state is PicLoadedState) {
            return Card(
              margin: const EdgeInsets.all(10),
              child: Container(
                padding: const EdgeInsets.all(20),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.network(
                      pic.url,
                      width: 150,
                      height: 150,
                    ),
                    RichText(
                      text: TextSpan(
                        style: const TextStyle(color: Colors.black),
                        children: [
                          const TextSpan(
                              text: 'albumID: ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(text: '${pic.albumId}')
                        ],
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        style: const TextStyle(color: Colors.black),
                        children: [
                          const TextSpan(
                              text: 'ID картинки: ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(text: '${pic.id}')
                        ],
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        style: const TextStyle(color: Colors.black),
                        children: [
                          const TextSpan(
                              text: 'Заголовок: ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(text: pic.title)
                        ],
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        style: const TextStyle(color: Colors.black),
                        children: [
                          const TextSpan(
                              text: 'Ссылка на картинку: ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(text: pic.url)
                        ],
                      ),
                    ),
                    ElevatedButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text('НАЗАД'))
                  ],
                ),
              ),
            );
          }
          return const Center(
            child: CircularProgressIndicator.adaptive(),
          );
        },
      ),
    );
  }
}
