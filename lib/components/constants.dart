const int num_categories = 4;
const int num_pics = 6;

const String my_img_url =
    'https://sun9-41.userapi.com/impg/Tg9drYBzHuByC4VTDR0jlnXrmXYpQ9D5kzq89Q/hpvQ675u2QI.jpg?size=810x1080&quality=96&sign=721fd0e76c4a52fedea317348fb89c80&type=album';

const String my_vk_url = 'https://vk.com/confirmedancient';
const String my_youtube_url =
    'https://www.youtube.com/channel/UCvhmhEFDZo7PnUf3MS2oJOQ';
const String my_instagram_url = 'https://www.instagram.com/zhiga_stud';

const List<String> images = [
  'https://sun9-40.userapi.com/impg/SyEyHVS_r02-Z3Nn-z_iJpoSEwR45fgraaAluw/28y107RsSSU.jpg?size=1080x1080&quality=96&sign=94eafb378a9cb31aac344dd8d12f2747&type=album',
  'https://sun9-4.userapi.com/impg/HivXvSaMIaQOg8qV-MlOp01tYXZV2S-EmZ8lDA/zL8iYXX4OkI.jpg?size=1280x960&quality=96&sign=1bcc26a1515329cec54e1a1b50fdd860&type=album',
  'https://sun9-63.userapi.com/impg/c855628/v855628148/19db54/ssZ8UkRXiSg.jpg?size=1280x1208&quality=96&sign=7da28960dea2d6ff523782cc70877993&type=album',
  'https://sun9-70.userapi.com/impg/Ndi4qLJG-mWXR5kBYGaGJt1vGZefS73-bJCWdQ/JrKUA8MEvEM.jpg?size=1200x1200&quality=96&sign=b72b08a437da3eaeb073d1e98dff3007&type=album',
  'https://sun9-17.userapi.com/impg/YLkGDgV_haXktnTbmezrfK_8hp7e3A-SK_aWfg/XCuPOhNlpMk.jpg?size=1280x1280&quality=96&sign=d18b711bf4e7ee1d42942e1286e1e50c&type=album',
  'https://sun9-45.userapi.com/impg/c855436/v855436540/17b9b1/5Wtf4iTLYdc.jpg?size=1280x960&quality=96&sign=1ebd4834b5d5bceafe50e4ce4f8b092e&type=album',
];
