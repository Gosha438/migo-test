import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';

class ImgCard extends StatelessWidget {
  final String url;
  const ImgCard({Key key, this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExtendedImage.network(
      url,
      shape: BoxShape.rectangle,
      borderRadius: const BorderRadius.all(Radius.circular(20)),
      width: 130,
      height: 130,
      fit: BoxFit.cover,
    );
  }
}
