import 'package:flutter/material.dart';
import 'package:migo_test/components/constants.dart';
import 'package:migo_test/models/pic.dart';
import 'package:migo_test/widgets/pic_card.dart';

class CategoryCard extends StatelessWidget {
  const CategoryCard({@required this.categoryId, @required this.pics});

  final List<Pic> pics;
  final int categoryId;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: const EdgeInsets.all(10),
          child: Text(
            'Категория №$categoryId',
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        GridView.count(
          crossAxisCount: 2,
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: List.generate(num_pics, (i) {
            return PicCard(pic: pics[i]);
          }),
        ),
      ],
    );
  }
}
