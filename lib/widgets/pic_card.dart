import 'package:flutter/material.dart';
import 'package:migo_test/models/pic.dart';
import 'package:migo_test/pages/pic_info.dart';

class PicCard extends StatelessWidget {
  const PicCard({@required this.pic});

  final Pic pic;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => PicInfoPage(
            pic: pic,
          ),
        ),
      ),
      child: Container(
        padding: const EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width / 2,
        height: MediaQuery.of(context).size.width / 2,
        child: Image.network(pic.url),
      ),
    );
  }
}
