class Pic {
  int albumId;
  int id;
  String title;
  String url;
  String thumbnailUrl;

  Pic({
    this.albumId,
    this.id,
    this.title,
    this.url,
    this.thumbnailUrl,
  });

  Map<String, dynamic> toMap() {
    return {
      'albumId': albumId,
      'id': id,
      'title': title,
      'url': url,
      'thumbnailUrl': thumbnailUrl,
    };
  }

  Pic.fromMap(Map<String, dynamic> data) {
    albumId = data['albumId'] as int;
    id = data['id'] as int;
    title = data['title'].toString();
    url = data['url'].toString();
    thumbnailUrl = data['thumbnailUrl'].toString();
  }
}
