import 'package:flutter/material.dart';
import 'package:migo_test/models/pic.dart';

abstract class PicsState {}

class PicsLoadingState extends PicsState {}

class PicsLoadedState extends PicsState {
  List<Pic> pics;
  PicsLoadedState({@required this.pics}) : super();
}

class PicsErrorState extends PicsState {}
