import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:migo_test/bloc/picsBloc/pics_event.dart';
import 'package:migo_test/bloc/picsBloc/pics_state.dart';
import 'package:migo_test/components/constants.dart';
import 'package:migo_test/models/pic.dart';
import 'package:migo_test/services/pics_provider.dart';

class PicsBloc extends Bloc<PicsEvent, PicsState> {
  PicsBloc() : super(null);

  PicsState get initialState => PicsLoadingState();
  final picsProvider = PicsProvider();
  @override
  Stream<PicsState> mapEventToState(PicsEvent event) async* {
    if (event is PicsLoadEvent) {
      yield PicsLoadingState();
      try {
        final List<Pic> pics =
            await picsProvider.loadPics(0, num_pics * num_categories);
        yield PicsLoadedState(pics: pics);
      } catch (_) {
        yield PicsErrorState();
      }
    }
  }
}
