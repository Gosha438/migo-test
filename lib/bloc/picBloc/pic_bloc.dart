import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:migo_test/bloc/picBloc/pic_event.dart';
import 'package:migo_test/bloc/picBloc/pic_state.dart';

class PicBloc extends Bloc<PicEvent, PicState> {
  PicBloc() : super(null);

  @override
  Stream<PicState> mapEventToState(PicEvent event) async* {
    if (event is PicLoadEvent) {
      yield PicLoadingState();
      await Future.delayed(const Duration(milliseconds: 500));
      yield PicLoadedState();
    }
  }
}
