import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:migo_test/components/constants.dart';
import 'package:migo_test/models/pic.dart';

class PicsProvider {
  Future<List<Pic>> loadPics(int start, int limit) async {
    await Future.delayed(const Duration(milliseconds: 500));
    final response = await http.get(Uri.parse(
        'https://jsonplaceholder.typicode.com/photos?_start= $start &_limit= $limit'));
    if (response.statusCode == 200) {
      final List<dynamic> pictures = json.decode(response.body) as List;
      return pictures
          .getRange(0, num_categories * num_pics)
          .map((e) => Pic.fromMap(e as Map<String, dynamic>))
          .toList();
    } else {
      throw Exception('not loaded');
    }
  }
}
